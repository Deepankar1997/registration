$(function(){
	var $formvalidate = $("#formvalidation");
	//$("#accordion").accordion(){
	//	collapsible:true
	//};		
	
	$.validator.addMethod("passcheck", function(value) {
			return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
	       	&& /[a-z]/.test(value) // has a lowercase letter
	       	&& /\d/.test(value) // has a digit
	       	&& /^(?=.*[!@*])/.test(value) // has a special character
	       },"Password not strong.Please enter atleast one digit,one letter and one special character('!','@','*')");

	$.validator.addMethod("space_not_allowed", function(value, element){
		return value == '' || value.trim().length != 0
	},"spaces not allowed.");

	$('#acc').click(function(){
		$('.accordion').toggle();
	});
	$('#submitform').click(function(){
		alert("Are you sure to register for this event?");
	})

	if($formvalidate.length){
		$formvalidate.validate({
			rules:{
				fname: {
					required: true,
					space_not_allowed: true
				},
				lname: {
					required: true,
					space_not_allowed: true
				},
				age: {
					required: true,
					range: [18,120]
				},
				address: {
					required: true
				},
				city: {
					required: true
				},
				emailaddress: {
					required: true,
					email: true,
					space_not_allowed: true
				},
				contactnumber: {
					required: true,
					range: [1000000000,9999999999]
				},
				username: {
					required: true
				},
				password1: {
					required: true,
					passcheck: true,
					space_not_allowed: true,
					minlength: 8,
					maxlength: 30
				},
				confirmpassword: {
					required: true,
					equalTo: '#pass'
				},
			},
			messages:{
				fname: {
					required: 'Please enter the first name',
				},
				lname: {
					required: 'Please enter the last name',
				},
				age: {
					required: 'Please enter the age',
					range: 'Minimum 18 years old and Maximum 120 years old'
				},				address: {
					required: 'Please enter the address'
				},
				city: {
					required: 'Please enter the city'
				},
				emailaddress: {
					required: 'Please enter the email address',
					email: 'Please enter a valid email address'
				},
				contactnumber: {
					required: 'Please enter the contact number',
					range: 'Please enter a valid 10-digit phone number'
				},
				username: {
					required: 'Please enter the username'
				},
				password1: {
					required: 'Please enter a valid password',
					minlength: 'Password must be of atleast 8 characters',
					maxlength: 'Password must be of atmost 30 characters'
				},
				confirmpassword: {
					required: 'Please confirm your password',
					equalTo: 'Please enter the same password as above'
				},
			},
		})
	}
})